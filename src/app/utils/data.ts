import {User} from "../model/user";

export const USER_LIST: User[]  = [
  {
    id: 1,
    nom: 'NOM1',
    prenom: 'Prenom1',
    addresse: 'Address 1'
  },
  {
    id: 2,
    nom: 'NOM2',
    prenom: 'Prenom2',
    addresse: 'Address 2'
  },
  {
    id: 3,
    nom: 'NOM3',
    prenom: 'Prenom3',
    addresse: 'Address 3'
  },
  {
    id: 4,
    nom: 'NOM4',
    prenom: 'Prenom4',
    addresse: 'Address 4'
  },
  {
    id: 5,
    nom: 'NOM5',
    prenom: 'Prenom5',
    addresse: 'Address 5'
  },
  {
    id: 6,
    nom: 'NOM6',
    prenom: 'Prenom6',
    addresse: 'Address 6'
  }
]

