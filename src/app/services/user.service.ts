import {Injectable} from '@angular/core';
import {User} from "../model/user";
import {USER_LIST} from "../utils/data";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private usersList: User[];

  constructor() {
  }

  getAllUsers(): User[] {
    this.usersList = USER_LIST;
    return this.usersList;
  }

  getUserById(id: number): User {
    return this.usersList.find(user => user.id === id);
  }
}
