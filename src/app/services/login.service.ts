import {Injectable} from '@angular/core';
import {Login} from "../utils/login";

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor() {
  }

  login(login: Login) {
    if (login.username === 'admin' && login.password === 'secret@Password00') {
      sessionStorage.setItem('userLogin', JSON.stringify(login));
      return true;
    } else {
      return false;
    }
  }
}
