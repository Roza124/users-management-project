import {Component, OnInit} from '@angular/core';
import {Login} from "../../../utils/login";
import {LoginService} from "../../../services/login.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  userLogin: Login;
  errorMessage: string;

  constructor(private loginService: LoginService, private router: Router) {
    this.userLogin = new Login();
  }

  ngOnInit(): void {
  }

  login() {
    const isLoggedIn = this.loginService.login(this.userLogin);
    if(isLoggedIn) {
       this.router.navigate(['users']);
    } else {
      this.errorMessage= 'Nom d\'utilisateur ou mot de passe incorrectes !';
    }
  }

}
