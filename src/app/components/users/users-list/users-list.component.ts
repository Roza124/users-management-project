import {Component, OnInit} from '@angular/core';
import {UserService} from "../../../services/user.service";
import {User} from "../../../model/user";

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {

  userList: User[];
  userDetails: User;

  constructor(private userService: UserService) {
  }

  ngOnInit(): void {
    this.userList = this.userService.getAllUsers();
  }

  getUserDetails(user: User) {
    this.userDetails = user;
  }

}
